<!-- SPDX-FileCopyrightText: no
     SPDX-License-Identifier: CC0-1.0
-->

# Calamares First-Run

> Calamares configuration and additional modules for first-run (in a Plasma DE) use

[Calamares](https://calamares.io/) is a distro-, desktop- and toolkit-agnostic independent installer for Linux systems. Linux distributions can use Calamares as part of their ISO to provide a graphical user interface to perform the installation. Calamares can **also** be used as a first-run wizard or a system-configuration application. This repository contains Calamares configurations and code specific to a first-run application on a KDE Plasma Desktop system.

## Getting Started

You will need:
- a build of Calamares with the development bits included (e.g. self-built or from a dev-package) plus all the tools needed to build it
- this repository.

Then:
- clone this repository,
- in a shell, navigate to the cloned repository,
- Create a build directory and run CMake there: `mkdir build ; cd build ; cmake ..`
- Then run make to build things: `make`

## Contributing

See the [contribution guide](CONTRIBUTING.md) for details.

## Join the Conversation

Regular Calamares development chit-chat happens in a [Matrix](https://matrix.org/)
room, `#calamares:kde.org`. The conversation is bridged with IRC
on [Libera.Chat](https://libera.chat/).
Responsiveness is best during the day
in Europe, but feel free to idle. If you use IRC, **DO NOT** ask-and-leave. Keep
that chat window open because it can easily take a few hours for
someone to notice a message.
Matrix is persistent, and we'll see your message eventually.

* [![Join us on Matrix](https://img.shields.io/badge/Matrix-%23calamares:kde.org-blue)](https://webchat.kde.org/#/room/%23calamares:kde.org)
* [![Chat on IRC](https://img.shields.io/badge/IRC-Libera.Chat%20%23calamares-green)](https://kiwiirc.com/client/irc.libera.chat/#calamares)
